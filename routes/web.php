<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Authentication\RegistrationController;
use App\Http\Controllers\Authentication\AuthenticationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('dashboard.index');
});


Route::prefix('dashboard')->group(function(){
    Route::get('index',[DashboardController::class, 'Dashboard'])->name('dashboard.index');
    Route::get('reports',[DashboardController::class, 'Reports'])->name('dashboard.reports');
});

Route::resource('products', ProductsController::class);

// Route::get('login', [AuthenticationController::class, 'Login'])->name('authentication.login');
// Route::get('forgot-password', [AuthenticationController::class, 'ForgotPassword'])->name('authentication.forgot-password');
// Route::get('reset-password/{id}', [AuthenticationController::class, 'ResetPassword'])->name('authentication.reset-password');
// Route::get('logout', [AuthenticationController::class, 'Logout'])->name('authentication.logout');