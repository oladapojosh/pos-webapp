<?php

namespace Database\Factories;

use App\Models\Product;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'product_name' => $this->faker->word,
            'product_description' => $this->faker->sentence,
            'product_tag' => $this->faker->sentence,
            'product_cost' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 50),
            'product_group_id' => $this->faker->numberBetween($min=1, $max=200),
            'product_plu_id' => $this->faker->numberBetween($min=1, $max=20),
            'product_category_id' => $this->faker->numberBetween($min=1, $max=50),
            'product_account_id' => $this->faker->numberBetween($min=1, $max=50),
            'product_stock_id' => $this->faker->numberBetween($min=1, $max=50),
            'product_rrp' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 50),

            'product_expiration_date' => $this->faker->dateTimeBetween($startDate ='now', $enddate='+2 years'),
            'product_vat' => $this->faker->randomElement($array = array (NULL, $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 30))),
            'product_gross_profit' => $this->faker->randomElement($array = array (NULL, $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 5000))),

        ];
    }
}
