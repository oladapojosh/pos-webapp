<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductPluFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {


        return [
            'product_std_price' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_2' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_3' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_4' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_5' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_6' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_7' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_8' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),
            'product_price_9' => json_encode(array($this->fake_array(),$this->fake_array(),$this->fake_array())),

        ];
    }
    public function fake_array(){
        return $this->faker->randomElement($array = array ($this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 300)));
    }
}
