<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('order_id');
            $table->bigInteger('order_user_id'); //employee
            $table->tinyInteger('order_status'); //cancelled/refunded
            $table->text('order_plan')->nullable();
            $table->tinyInteger('order_type')->nullable();
            $table->float('order_amount_received')->nullable(); //internal, External
            $table->dateTime('order_shipped_at')->nullable();
            $table->bigInteger('order_account_id');
            $table->text('order_scheme')->nullable();
            $table->text('stripe_payment_intent_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
