<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('receipt_id');
            $table->bigInteger('receipt_product_id');
            $table->bigInteger('receipt_variance_id')->nullable();

            $table->float('receipt_product_price')->nullable();
            $table->smallInteger('receipt_product_quantity')->nullable(); //cancelled/refunded
            $table->tinyInteger('receipt_product_status')->nullable(); //cancelled/refunded

            $table->bigInteger('receipt_order_id');
            $table->bigInteger('receipt_user_id'); //customer
            $table->bigInteger('receipt_plan_id')->nullable(); //customer
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
