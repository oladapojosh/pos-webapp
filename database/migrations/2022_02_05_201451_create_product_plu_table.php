<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPluTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_plu', function (Blueprint $table) {
            $table->bigIncrements('product_plu_id')->unsigned();
            $table->text('product_std_price')->nullable();
            $table->text('product_price_2')->nullable();
            $table->text('product_price_3')->nullable();
            $table->text('product_price_4')->nullable();
            $table->text('product_price_5')->nullable();
            $table->text('product_price_6')->nullable();
            $table->text('product_price_7')->nullable();
            $table->text('product_price_8')->nullable();
            $table->text('product_price_9')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_plu');
    }
}
