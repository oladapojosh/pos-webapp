<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('product_id')->unsigned();
            $table->string('product_name');
            $table->mediumText('product_description');
            $table->text('product_barcode')->nullable();
            $table->bigInteger('product_group_id');
            $table->bigInteger('product_plu_id')->nullable();
            $table->bigInteger('product_category_id');
            $table->text('product_cost')->nullable(); // as array
            $table->text('product_supplier')->nullable(); // as array
            $table->float('product_rrp')->nullable();
            $table->smallInteger('product_alert_level')->nullable();
            $table->string('product_image')->nullable();
            $table->bigInteger('product_account_id');
            $table->text('product_vat')->nullable();
            $table->date('product_expiration_date');
            $table->string('product_points')->nullable();
            $table->text('product_boolean')->nullable();
            $table->text('product_tag')->nullable();
            $table->text('product_gross_profit')->nullable();
            $table->text('product_stock')->nullable();
            $table->text('product_recipe')->nullable();
            $table->text('product_allergens')->nullable();
            $table->text('product_print_exclusion')->nullable();
            $table->text('product_web')->nullable();
            $table->text('product_stock_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
