<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id('setting_id');
            $table->bigInteger('setting_account_id');
            $table->string('setting_prefix')->nullable();
            $table->text('setting_product_category')->nullable();
            $table->text('setting_product_plu')->nullable();
            $table->text('setting_product_brand')->nullable();
            $table->text('setting_product_discount')->nullable();
            $table->string('setting_vat')->nullable();
            $table->text('setting_api')->nullable();
            $table->text('setting_reservation')->nullable();
            $table->text('setting_receipt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
