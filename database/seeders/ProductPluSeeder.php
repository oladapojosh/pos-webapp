<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ProductPlu;
use App\Models\Product;

class ProductPluSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ProductPlu::factory(20)->create();
    }
}
