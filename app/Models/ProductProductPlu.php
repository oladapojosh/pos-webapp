<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductProductPlu extends Model
{
    use HasFactory;

    public $incrementing = true;

    protected $table = 'product_product_plu';
    protected $primaryKey = 'product_product_plu_id';
}
