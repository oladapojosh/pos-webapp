<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    use HasFactory;
    protected $table = 'receipts';
    protected $primaryKey = 'receipt_id';


    public static function ReceiptProduct(){
        return [
          'product_id',
          'product_variance_id'
        ];
    }

}
