<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    // use SoftDeletes;

    protected $table = 'product';
    protected $primaryKey = 'product_id';



    public static function List($column,$filter){
        return Product::
        leftJoin('store', 'store.store_id', '=', 'product.product_account_id')
        ->where($column,  $filter)
        ->orderBy('product.created_at', 'desc');
    }

    public static function ListPlu(){
        return Product::
        leftJoin('product_plu','product_plu.product_plu_id','=','product.product_plu_id')
        ->orderBy('product.product_id','asc');
    }

    public static function QuantityDecrease(int $product_id, int $count){
        Product::decrement('quantity', $count, array('product_id' => $product_id));
    }

    public static function ProductCost(){
        return [
            'product_selling_cost',
            'product_quantity',
            'product_cost_is_default',
        ];
    }

    public static function ProductBoolean(){
        return [

            'product_is_global',
            'product_is_online',
            'product_alert_level',
            'product_is_disabled',

        ];
    }

    public static function ProductPoints(){
        return [
            'product_points_gain',
            'product_points_collect'
        ];
    }

    public static function ProductTag(){
        return [

        ];
    }

    public static function ProductRecipe(){
        return [
            'product_recipe_text',
            'product_recipe_type',
        ];
    }

    public static function ProductPrintExclusion(){
        return [

            'product_column_name',
            'product_exclusion_type',
            //'product_exclusiontable_id',
            //'product_exclusiontable_type',
        ];
    }



    public static function ProductExclusionType(){
        return [
            'invoice',
            'receipt'
        ];
    }

    public static function ProductRecipeType(){
        return [
            'description',
            'url'
        ];
    }

    public static function ProductGrossProfit(){
        return [
           'product_target',
           'product_actual',
           'product_average_cost',
           'product_rrp'
        ];
    }

    public static function ProductSupplier(){
        return [
           'supplier_id',
           'code',
           'case_cost',
           'unit_cost'
        ];
    }
}
