<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
    public function forgotPassword(Request $request){

        if ($request->has('email')) {

                //You can add validation login here
            $user = User::Email($request->input('email'))->first();
            if (!$user) {
                return redirect()->back()->withErrors(['email' => trans('User does not exist')]);
            }
            else{

                $link = URL::temporarySignedRoute(
                    'authentication.reset-password', now()->addMinutes(30), ['id' => $user->user_id]
                );

                Mail::to($request->input('email'))
                ->send(new \App\Mail\ResetPassword($user->person_firstname, $user->email, $link));

                if (Mail::failures()) {
                    return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
                } else {
                    return redirect()->back()->with('success', trans('A reset link has been sent to your email address.'));
                }
            }

        }
    return view('authentication.forgotPassword');

}

public function resetPassword(Request $request, $id){

    $user = User::find($id);

    if ($request->has('password_1') && $request->has('password_2')) {
        if ($request->input('password_1') == $request->input('password_2')) {
            $user->password = Hash::make($request->input('password_1'));
            $user->update(); //or $user->save();
            Auth::login($user);
            return redirect()->route('dashboard.index')->with('success', 'Password Reset');
        }else{
            return redirect()->back()->with('error', 'Password not the same');
        }

    }else{

        return view ('authentication.resetPassword', ['user' => $user]);
    }

}

public function login(Request $request){
    // Retrive Input
    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials)) {
        $request->session()->regenerate();
        // if success login
        return redirect()->route('dashboard.index');
    }

    // if failed login
    return view('authentication.login')->with('authentication.failed');
}

public function logout(Request $request){

   if (Auth::check()) {
        $request->session()->forget('user-'.Auth::user()->user_id);
        Auth::logout();
   }else{
        $request->session()->flush();
        Auth::logout();
   }



   return view('authentication.login');

}
}
