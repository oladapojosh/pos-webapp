
@php
use App\Models\Product;
@endphp

<div class="table-plu" uk-grid>

    <div class="uk-width-1-1">

        <table class="edit-table uk-table uk-table-small">
            <thead class="uk-text-color-secondary">
                <tr>
                    <th>PLU</th>
                    <th>Product</th>
                    <th>Price 1 L1</th>
                    <th>Price 2 L1</th>
                    <th>Price 3 L1</th>
                    <th>In Stock Qty</th>
                </tr>
            </thead>

            <tbody>
                @foreach($data['productPluList'] as $product)


                @if($product->product_id)


                @php
                    $product_prices = json_decode($product->product_std_price);
                @endphp
                <tr>
                    <td><a class="link uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" href="{{ route('products.show', $product->product_id) }}" title="View Record {{ $product->product_id }}">{{ $product->product_id }}</a></td>

                    <td class="field-text"><input type="text" name="plu_{{ $product->product_id }}_name" value="{{ $product->product_name }}" readonly="readonly" maxlength="24"></td>
                    <td class="field-price">£<input class="" type="text" name="plu_{{ $product->product_id }}_price1l1" value="{{ $product_prices[0] }}" size="4" readonly="readonly"></td>
                    <td class="field-price">£<input class="" type="text" name="plu_{{ $product->product_id }}_price2l1" value="{{ $product_prices[1] }}" size="4" readonly="readonly"></td>
                    <td class="field-price">£<input class="" type="text" name="plu_{{ $product->product_id }}_price3l1" value="{{ $product_prices[2] }}" size="4" readonly="readonly"></td>
                    <td class="field-stock">-758.000</td>
                    <td></td>
                </tr>

                @endif
                @endforeach
            </tbody>
        </table>

    </div>


</div>