@extends('layout.dashboard_master')
@section('content')
<div uk-sticky uk-height-match class="ps-navbar-container uk-navbar-container">
    <div class="uk-container uk-container-expand">
        <nav uk-navbar>
            <div class="uk-navbar-left">
                <a href="#" class="ps-navbar uk-navbar-item uk-logo">
                    {{ config('app.name') }}
                </a>
            </div>
            <div class="uk-navbar-right uk-dark">
                <ul class="uk-navbar-nav">
                    <li class="uk-active">
                        <a href="#"><span class="fa fa-question-circle"></span> Help</a>
                        <div uk-dropdown="pos: bottom-right; mode: click; offset: 0;" class="uk-padding-remove">
                            <div class="uk-card">
                                <div class="uk-card-header uk-background-muted uk-margin-remove">
                                    <h3 class="uk-card-title">
                                        Support details
                                    </h3>
                                </div>
                                <div class="uk-column-1-2 uk-padding-small">
                                    <div>
                                        <p>
                                            {{ config('app.name') }}
                                        </p>
                                    </div>

                                    <div>
                                        <p>
                                            Tel: <a href="tel:01-3673822">01-3673822</a>
                                        </p>

                                        <p>
                                            <a href="mailto:support@icrtouch.com?subject=TouchOffice Web Support Request - Alias: touchofficeweb">support@icrtouch.com</a>
                                        </p>
                                        <p>
                                            Your company alias: touchofficeweb
                                        </p>

                                    </div>

                                </div>
                            </div>


                        </div>
                    </li>

                    <li class="uk-active">
                        <a href="#"><span class="fa fa-user"></span> name</a>
                        <div uk-dropdown="pos: bottom-right; mode: click; offset: -17;">
                           <ul class="uk-nav uk-navbar-dropdown-nav">
                               <li class="uk-nav-header">Settings</li>


                               <li><a href="#">Logout</a></li>
                           </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>

<div id="sidebar" class="tm-sidebar-left ps-bg">

    @include('includes.sidebar')

</div>
<div class="content-padder content-background">
    <div class="uk-section-small">
        <div class="uk-container uk-container-large">
            <div class="uk-card uk-card-default uk-card-body ps-admin-bg">
                <div class="uk-grid-column-small uk-grid-row-large uk-child-width-expand" uk-grid>
                    <div class="uk-width-medium">

                            <form action="" method="post">
                            <select class="uk-select" id="ps_site-list" name="ps_site-list">
                                <option value="Head Office">0- Head Office</option>
                                <option value="JS Office">1- JS Office</option>
                            </select>
                        </form>
                    </div>


                </div>
                <div class="" uk-grid>
                    <div class="uk-width-auto@m">


                            <button class="uk-button uk-button-default ps-btn-cv">Save</button>
                            <button class="uk-button uk-btn-green ps-text-light ps-btn-cv">New</button>
                            <button class="uk-button uk-button-secondary ps-btn-cv">Range</button>

                    </div>
                    <div class="uk-width-1-3">

                    </div>
                    <div class="uk-width-expand@m">
                        <div class="uk-button-group">
                        <button class="uk-button uk-button-default">Live Edit</button>
                        <button class="uk-button uk-button-primary padding-remove-left">Schedule</button>
                        </div>
                    </div>
                </div>
                <div uk-grid>

                    <div class="uk-width-1-3">
                        <h4>PLU Programming</h4>
                    </div>

                </div>
                <div uk-grid>

                    <div class="uk-width-1-2">
                        <button class="uk-button uk-button-default ps-btn-cv">Search</button>
                        <span id="jump2rec" class="uk-flex-left">
                            Jump to PLU

                            <input type="text" size="4" maxlength="5" data-file="1" data-url="//www.touchoffice.net/apps/editpludetails?rec=" data-search="plu" data-error="PLU not found" class="">
                        </span>


                    </div>
                    <div class="uk-width-expand uk-margin-right-remove">
                        <a title="" href=""><span style="height: 25px; width: 25px;  float:right;" uk-icon="cog"></span></a>
                    </div>

                </div>

                <div class="table-plu" uk-grid>

                    <div class="uk-width-1-1">

                        <h3 id="pludetailsname">

                            <!-- PLU name input - Autosuggest similar plu names -->
                            <div class="inner-wrap">
                                <div class="uk-margin input-wrap">
                                    <div class="uk-inline">

                                            <span class="uk-form-icon uk-form-icon-flip input-clear" uk-icon="icon: chevron-right"></span>

                                        <div class="input-wrap">
                                            <input class="uk-form-large uk-form-width-large" type="text" autocomplete="new-password" maxlength="24" id="input_plu_name" name="plu_2_name" value="Carlin" placeholder="Enter product name" style="background-color: rgb(255, 255, 255); border: 1px solid rgb(235, 90, 0);">
                                            <div class="input-drop-down" id="autocomplete_plu_name">
                                                {{-- <span>Name Suggestions</span>
                                                <div class="suggestions"></div> --}}

                                                </div>

                                        </div>

                                    </div>




                                </div>
                            </div>
                            <span>(2)</span>
                        </h3>
                    </div>


                </div>
            </div>

        </div>
    </div>

</div>

@endsection
@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.uikit.min.css">
@endsection
@push('scripts')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.uikit.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="uikit/dist/js/uikit-icons.min.js"></script>
<script>
    $(document).ready(function(){
        $("#ps_site-list").select2();
        $("#group-product-table").DataTable({
            paging:false,
        });
        $("#dpt-product-table").DataTable({
            paging:false,
        });
        $('#ps_site-list').on('select2:opening select2:closing', function( event ) {
    var $searchfield = $(this).parent().find('.select2-search__field');
    $searchfield.attr('placeholder', 'Search this site');
});
});


</script>
@endpush