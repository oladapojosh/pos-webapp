<ul class="uk-nav uk-nav-default sidebar-nav">

    <li class="menu-home {{ Request::url() == route('dashboard.index') ? 'selected' : '' }}" ><a href="{{ route('dashboard.index') }}"><span class="fa fa-home uk-margin-small-right"></span> Home</a></li>
    <li class="menu-report {{ Request::url() == route('dashboard.reports') ? 'selected' : '' }}"><a href="{{ route('dashboard.reports') }}"><span class="fa fa-chart-line uk-margin-small-right"></span> Reports</a></li>
    <li class="menu-products {{ Request::url() == route('products.index') ? 'selected' : '' }}"><a href="{{ route('products.index') }}"><span class="fa fa-tag uk-margin-small-right"></span> Products</a></li>

</ul>