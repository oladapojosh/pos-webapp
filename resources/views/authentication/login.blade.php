@extends('layout.master')

@section('content')


<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <div class="card px-5 py-5" id="form1">
                <div class="form-data">
                    <div class="mb-4"> <span>Email</span>
                        <input name="email" type="text" class="form-control">
                        <div class="invalid-feedback">A valid email is required!</div>
                    </div>
                    <div class="mb-4"> <span>Password</span> <input name="password" type="password" class="form-control">
                        <div class="invalid-feedback">Password must be 8 character!</div>
                    </div>
                    <div class="mb-3"> <button class="btn btn-dark w-100">Login</button> </div>
                </div>
                <div class="success-data d-none">
                    <div class="text-center d-flex flex-column"> <i class='bx bxs-badge-check'></i> <span class="text-center fs-1">You have been logged in <br> Successfully</span> </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection