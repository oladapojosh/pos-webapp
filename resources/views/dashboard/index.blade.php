@extends('layout.dashboard_master')
@section('content')
<div uk-sticky uk-height-match class="ps-navbar-container uk-navbar-container">
    <div class="uk-container uk-container-expand">
        <nav uk-navbar>
            <div class="uk-navbar-left">
                <a href="#" class="ps-navbar uk-navbar-item uk-logo">
                    {{ config('app.name') }}
                </a>
            </div>
            <div class="uk-navbar-right uk-dark">
                <ul class="uk-navbar-nav">
                    <li class="uk-active">
                        <a href="#"><span class="fa fa-question-circle"></span> Help</a>
                        <div uk-dropdown="pos: bottom-right; mode: click; offset: 0;" class="uk-padding-remove">
                            <div class="uk-card">
                                <div class="uk-card-header uk-background-muted uk-margin-remove">
                                    <h3 class="uk-card-title">
                                        Support details
                                    </h3>
                                </div>
                                <div class="uk-column-1-2 uk-padding-small">
                                    <div>
                                        <p>
                                            {{ config('app.name') }}
                                        </p>
                                    </div>

                                    <div>
                                        <p>
                                            Tel: <a href="tel:01-3673822">01-3673822</a>
                                        </p>

                                        <p>
                                            <a href="mailto:support@icrtouch.com?subject=TouchOffice Web Support Request - Alias: touchofficeweb">support@icrtouch.com</a>
                                        </p>
                                        <p>
                                            Your company alias: touchofficeweb
                                        </p>

                                    </div>

                                </div>
                            </div>


                        </div>
                    </li>

                    <li class="uk-active">
                        <a href="#"><span class="fa fa-user"></span> name</a>
                        <div uk-dropdown="pos: bottom-right; mode: click; offset: -17;">
                           <ul class="uk-nav uk-navbar-dropdown-nav">
                               <li class="uk-nav-header">Settings</li>


                               <li><a href="#">Logout</a></li>
                           </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>

<div id="sidebar" class="tm-sidebar-left ps-bg">

    @include('includes.sidebar')

</div>
<div class="content-padder content-background">
    <div class="uk-section-small">
        <div class="uk-container uk-container-large">
            <div class="uk-card uk-card-default uk-card-body ps-admin-bg">
                <div class="uk-grid-column-small uk-grid-row-large uk-child-width-expand" uk-grid>
                    <div class="uk-width-medium">

                            <form action="" method="post">
                            <select class="uk-select" id="ps_site-list" name="ps_site-list">
                                <option value="Head Office">0- Head Office</option>
                                <option value="JS Office">1- JS Office</option>
                            </select>
                        </form>
                    </div>
                    <div>

                        <nav class="uk-navbar-container" uk-navbar>
                            <div class="uk-navbar-right">
                            <ul class="uk-navbar-nav">

                                <li class="uk-active"><a class="uk-button uk-button-default" href="">Today</a></li>
                                <li><a class="uk-button uk-button-default" href="">Yesterday</a></li>
                                <li><a class="uk-button uk-button-default" href="">This Week</a></li>
                                <li><a class="uk-button uk-button-default" href="">Last Week</a></li>
                                <li><a class="uk-button uk-button-default" href="">This Month</a></li>
                                <li><a class="uk-button uk-button-default" href="">Last Month</a></li>
                                <li><a class="uk-button uk-button-default" href="">This Quarter</a></li>
                                <li><a class="uk-button uk-button-default" href="">Last Quarter</a></li>
                            </ul>
                        </div>
                        </nav>


                    </div>

                </div>
                <form class="uk-margin-medium">
                <div class="uk-grid-column-small uk-grid-row-large" uk-grid>
                    <div class="uk-width-1-2">
                        <div class="uk-form-horizontal">
                            <div class="uk-margin">
                                <label class="uk-form-label" for="form-horizontal-text">Start Date</label>
                                <div class="uk-form-controls">

                                    <input id="form-horizontal-text" name="filter-startdate" type="text" class="uk-input">
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="uk-width-1-2">
                        <div class="uk-form-horizontal">
                            <div class="uk-margin">
                                <label class="uk-width-auto uk-form-label" for="form-horizontal-text">End Date:</label>
                                <div class="uk-form-controls">

                                    <input id="form-horizontal-text" name="filter-startdate" type="text" class="uk-input">
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="uk-grid-column-small uk-grid-row-large uk-margin-small" uk-grid>
                    <div class="uk-width-1-2">
                        <div class="uk-form-horizontal">
                            <div class="uk-margin">
                                <label class="uk-form-label uk-width-auto" for="form-horizontal-text">Start Time</label>
                                <div class="uk-form-controls">

                                    <input id="form-horizontal-text" name="filter-startdate" type="text" class="uk-input">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="uk-width-1-2">
                        <div class="uk-form-horizontal">
                            <div class="uk-margin">
                                <label class="uk-width-small uk-form-label" for="form-horizontal-text">End Time:</label>
                                <div class="uk-form-controls">

                                    <input id="form-horizontal-text" name="filter-startdate" type="text" class="uk-input">
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="uk-grid-column-small uk-grid-row-large uk-margin-small" uk-grid>
                    <div class="uk-width-1-2">
                        <div class="uk-form-horizontal">
                            <div class="uk-margin-small">
                                <label class="uk-form-label" for="form-horizontal-text">Clerks</label>
                                <div class="uk-form-controls">
                                    <select class="uk-select" id="form-horizontal-text ps_clerk-list" name="ps_clerk-list">
                                        <option value="">All clerks</option>
                                        <option value="John">John</option>
                                    </select>


                                </div>
                            </div>

                        </div>

                </div>


                </div>

                <div class="uk-grid-column-small uk-grid-row-large uk-margin-small" uk-grid>
                    <div class="uk-width-1-2 uk-align-right">
                        <div class="uk-form-horizontal">
                            <div class="uk-margin-small">

                                <div class="uk-form-controls">

                                    <button type="reset" class="uk-button uk-button-default">Reset</button>
                                    <button type="submit" class="uk-button uk-button-primary uk-btn-green">Submit</button>
                                </div>
                            </div>

                        </div>

                </div>


                </div>

            </form>


                <div class="uk-grid-column-small uk-grid-row-large uk-child-width-expand" uk-sortable="handle: .uk-sortable-handle" uk-grid>
                    <div class="uk-width-1-2">
                        <div class="uk-card ps-border-cv">
                            <div class="uk-card-header uk-background-secondary uk-sortable-handle  uk-padding-remove ps-border-cv">
                                <p class="uk-card-title ps-text-light uk-text-small ps-padding-5">
                                    GROUP SALES TOTAL
                                </p>
                            </div>
                            <div class="uk-card-body">
                                <div id="group-product-table">
                                    <table class="uk-table uk-table-striped">
                                        <thead>
                                            <tr>
                                                <th>Group</th>
                                                <th>Quantity</th>
                                                <th>Total(&#163;)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Table Data</td>
                                                <td>Table Data</td>
                                                <td>Table Data</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="uk-card-footer uk-background-muted">
                                <button class="uk-button uk-button-default">Export to CSV</button>
                                <button class="uk-button uk-button-secondary">Export to PDF</button>


                            </div>
                        </div>

                    </div>
                    <div class="uk-width-1-2">
                        <div class="uk-card">
                            <div class="uk-card-header uk-background-secondary uk-sortable-handle uk-padding-remove ps-border-cv">
                                <p class="uk-card-title ps-text-light uk-text-small ps-padding-5">
                                   DEPARTMENT SALES TOTAL
                                </p>
                            </div>
                            <div class="uk-card-body">
                                <div id="dpt-product-table">
                                    <table class="uk-table uk-table-striped">
                                        <thead>
                                            <tr>
                                                <th>Department</th>
                                                <th>Quantity</th>
                                                <th>Total(&#163;)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Table Data</td>
                                                <td>Table Data</td>
                                                <td>Table Data</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="uk-card-footer uk-background-muted">
                                <button class="uk-button uk-button-default">Export to CSV</button>
                                <button class="uk-button uk-button-secondary">Export to PDF</button>


                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>

@endsection
@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.uikit.min.css">
@endsection
@push('scripts')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.uikit.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function(){
        $("#ps_site-list").select2();
        $("#group-product-table").DataTable({
            paging:false,
        });
        $("#dpt-product-table").DataTable({
            paging:false,
        });
        $('#ps_site-list').on('select2:opening select2:closing', function( event ) {
    var $searchfield = $(this).parent().find('.select2-search__field');
    $searchfield.attr('placeholder', 'Search this site');
});
});


</script>
@endpush